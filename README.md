# TP2-BDR


## Description

Travail 2 pour le cours 8INF803 - Bases de données réparties.
L'objectif de ce TP est de crawler un site web Donjons & Dragons afin de construire un outil de recherche de créatures.

## Pour commencer

### Dépendances

* Pyspark (https://spark.apache.org/docs/latest/api/python/)
* Tkinter
* BeautifulSoup4
* lxml

### Exécuter le programme

* Pour exécuter la partie 1 du TP

Pour lancer le crawler : 

```bash
$ python crawler.py
```

Pour faire l'indexage inversé :
```bash
$ python inverted_index.py
```

* Pour exécuter la partie 2 du TP

Pour lancer le crawler : 
```bash
$ python advanced_monster_crawler.py
```

Pour lancer l'outil de recherche (PathFinder) : 
```bash
$ python main.py
```

## Authors

Vincent Gagnon  
Hans Darmstadt-Bélanger  
Hilde Dokou  
Pierre Lefebvre  
