from bs4 import BeautifulSoup
import requests
import json

monsters = []


def parse_monster_name(monster_name):
    stop_words = ["(3pp)", "(3pp)", "(3PP)", "[3pp]", "; 3pp", "3pp;", ", 3pp", "-3PP", "(3PP-FGG)", "(3pp:TOHC)",
                  "(CR+2)"]
    if "Page Not Found" in monster_name:
        return None
    for word in stop_words:
        monster_name = monster_name.replace(word, "")
    monster_name = monster_name.replace('\u2019', "'")
    if ',' in monster_name and (monster_name.find('(') > monster_name.find(',') or monster_name.find('(') == -1):
        monster_name = monster_name.split(',')[1]
    return monster_name.strip(" ")


def parse_monster_description(monster_description):
    if monster_description:
        return monster_description.get_text()
    else:
        return ""


def parse_monster_type(soup):
    monster_type = soup.find('a', href=lambda
        href: href and "https://www.d20pfsrd.com/bestiary/rules-for-monsters/creature-types" in href)
    if monster_type:
        return monster_type.get_text()
    else:
        return ""


def parse_monster_spell(soup):
    monster_spells = []

    spells = soup.find_all('a', {'class': 'spell'})
    if spells:
        for spell in spells:
            spell = spell.get_text()
            spell = spell.replace('\u2019', "'").replace('\/', ' ').strip(' ')
            if spell:
                spell = spell[0].lower() + spell[1:]
            monster_spells.append(spell)
        return monster_spells

    else:
        return []


def parse_monster_speed(soup):
    if soup.find('b', text='Speed'):
        try:
            speed = soup.find('b', text='Speed').next_sibling.get_text().split(' ')[1]
            speed = int(speed)
            return speed
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_melee(soup):
    return True if soup.find('b', text='Melee') else False


def parse_monster_ranged(soup):
    return True if soup.find('b', text='Ranged') else False


def parse_monster_special_attacks(soup):
    monster_special_attacks = []

    special_attacks = soup.find('b', text='Special Attacks')
    if special_attacks:
        attacks = special_attacks.next_sibling.get_text().split(',')
        for attack in attacks:
            attack = attack.strip(" ")
            monster_special_attacks.append(attack)

        monster_special_attacks = list(filter(lambda val: val != "", monster_special_attacks))
        return monster_special_attacks
    else:
        return []


def parse_monster_ac(soup):
    if soup.find('b', text='AC'):
        try:
            ac = soup.find('b', text='AC').next_sibling.get_text().split(',')[0].strip(" ")
            ac = int(ac)
            return ac
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_hp(soup):
    if soup.find('b', text='hp'):
        try:
            hp = soup.find('b', text='hp').next_sibling.get_text().split(" ")[1]
            hp = int(hp)
            return hp
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_immunities(soup):
    monster_immunities = []

    immunities = soup.find('b', text='Immune')
    if immunities:
        immunities = immunities.next_sibling.get_text().split(',')
        for immunity in immunities:
            immunity = immunity.strip(" ").strip(";")
            monster_immunities.append(immunity)

        monster_immunities = list(filter(lambda val: val != "", monster_immunities))
        return monster_immunities
    else:
        return []


def parse_monster_str(soup):
    if soup.find('b', text='Str'):
        try:
            strength = soup.find('b', text='Str').next_sibling.get_text().strip(" ").strip(',').strip('\n')
            strength = int(strength)
            return strength
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_dex(soup):
    if soup.find('b', text='Dex'):
        try:
            dex = soup.find('b', text='Dex').next_sibling.get_text().strip(" ").strip(',').strip('\n')
            dex = int(dex)
            return dex
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_con(soup):
    if soup.find('b', text='Con'):
        try:
            con = soup.find('b', text='Con').next_sibling.get_text().strip(" ").strip(',').strip('\n')
            con = int(con)
            return con
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_int(soup):
    if soup.find('b', text='Int'):
        try:
            intellect = soup.find('b', text='Int').next_sibling.get_text().strip(" ").strip(',').strip('\n')
            intellect = int(intellect)
            return intellect
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_wis(soup):
    if soup.find('b', text='Wis'):
        try:
            wis = soup.find('b', text='Wis').next_sibling.get_text().strip(" ").strip(',').strip('\n')
            wis = int(wis)
            return wis
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_cha(soup):
    if soup.find('b', text='Cha'):
        try:
            wis = soup.find('b', text='Cha').next_sibling.get_text().strip(" ").strip(',').strip('\n')
            wis = int(wis)
            return wis
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_languages(soup):
    monster_languages = []

    languages = soup.find('b', text='Languages')
    if languages:
        languages = languages.next_sibling.get_text().split(',')
        for language in languages:
            language = language.strip(" ").strip(";")
            monster_languages.append(language)

        monster_languages = list(filter(lambda val: val != "", monster_languages))
        return monster_languages

    else:
        return []


def parse_monster_sr(soup):
    if soup.find('b', text='SR'):
        try:
            sr = soup.find('b', text='SR').next_sibling.get_text().strip(" ").strip(',').strip('\n')
            sr = int(sr)
            return sr
        except ValueError:
            return 0
        except AttributeError:
            return -1

    else:
        return -1


def parse_monster_stats(link):
    page = requests.get(link)
    soup = BeautifulSoup(page.content, 'lxml')

    monster_name = parse_monster_name(soup.find('h1').get_text())

    if monster_name:
        monster_description = parse_monster_description(soup.find('p', {'class': 'description'}))

        monster_type = parse_monster_type(soup)

        monster_spells = parse_monster_spell(soup)

        monster_speed = parse_monster_speed(soup)

        monster_melee = parse_monster_melee(soup)

        monster_ranged = parse_monster_ranged(soup)

        monster_special_attacks = parse_monster_special_attacks(soup)

        monster_ac = parse_monster_ac(soup)

        monster_hp = parse_monster_hp(soup)

        monster_immunities = parse_monster_immunities(soup)

        monster_str = parse_monster_str(soup)

        monster_dex = parse_monster_dex(soup)

        monster_con = parse_monster_con(soup)

        monster_int = parse_monster_int(soup)

        monster_wis = parse_monster_wis(soup)

        monster_cha = parse_monster_cha(soup)

        monster_languages = parse_monster_languages(soup)

        monster_sr = parse_monster_sr(soup)

        new_monster = {"name": monster_name, "description": monster_description, "type": monster_type,
                       "spells": monster_spells,
                       "link": link, "speed": monster_speed, "melee": monster_melee, "ranged": monster_ranged,
                       "special_attacks": monster_special_attacks, "AC": monster_ac, "HP": monster_hp,
                       "immunities": monster_immunities, "STR": monster_str, "DEX": monster_dex,
                       "CON": monster_con, "INT": monster_int, "WIS": monster_wis, "CHA": monster_cha,
                       "languages": monster_languages, "SR": monster_sr}

        print(json.dumps(new_monster, indent=4))

        monsters.append(new_monster)


def find_monsters(link):
    page = requests.get(link.get('href'))
    soup_bis = BeautifulSoup(page.content, 'lxml')
    for td in soup_bis.find_all('td'):
        for h3 in td.find_all('h3'):
            h3.extract()
        for sup in td.find_all('sup'):
            sup.extract()
        for a in td.find_all('a', href=lambda href: href and "https://www.d20pfsrd.com/bestiary/" in href):
            parse_monster_stats(a.get('href'))


def main():
    main_page = requests.get('https://www.d20pfsrd.com/bestiary/bestiary-alphabetical/')
    soup = BeautifulSoup(main_page.content, 'lxml')

    href_list = soup.find('ul', {'class': 'ogn-childpages'})

    for link in href_list.find_all('a'):
        find_monsters(link)

    with open('./results/advanced_monsters_results.json', 'w') as fout:
        fout.write('[' + ',\n'.join(json.dumps(monster) for monster in monsters) + ']\n')


if __name__ == "__main__":
    main()
