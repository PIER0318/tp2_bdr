from pyspark.sql.functions import array_contains, col, explode


# --- Fonctions Filtrage DF ---

def filter_name(df, substring):
    return df.filter(df.name.contains(substring))


def filter_description(df, substring):
    return df.filter(df.description.contains(substring))


def filter_type(df, types):
    typeList = []
    for key in types:
        if types[key].get() != "":
            typeList.append(types[key].get())
    # print(typeList)
    return df.filter(df.type.isin(typeList))


def filter_speed(df, min, max):
    return df.filter((df.speed >= min) & (df.speed <= max))


def filter_range(df, melee, ranged):
    return df.filter((df.melee == melee) & (df.ranged == ranged))


def filter_special_attacks(df, substring):
    return df.select("*", explode(col("special_attacks")).alias("tmp")) \
        .filter(col("tmp").contains(substring)) \
        .drop("tmp") \
        .distinct()


def filter_spells(df, substring):
    return df.select("*", explode(col("spells")).alias("tmp")) \
        .filter(col("tmp").contains(substring)) \
        .drop("tmp") \
        .distinct()


def filter_ac(df, min, max):
    return df.filter((df["AC"] >= min) & (df["AC"] <= max))


def filter_hp(df, min, max):
    return df.filter((df["HP"] >= min) & (df["HP"] <= max))


def filter_sr(df, min, max):
    return df.filter((df["SR"] >= min) & (df["SR"] <= max))


def filter_immunities(df, immunity):
    return df.filter(array_contains(col("immunities"), immunity))


def filter_str(df, min, max):
    return df.filter((df["STR"] >= min) & (df["STR"] <= max))


def filter_dex(df, min, max):
    return df.filter((df["DEX"] >= min) & (df["DEX"] <= max))


def filter_con(df, min, max):
    return df.filter((df["CON"] >= min) & (df["CON"] <= max))


def filter_int(df, min, max):
    return df.filter((df["INT"] >= min) & (df["INT"] <= max))


def filter_wis(df, min, max):
    return df.filter((df["WIS"] >= min) & (df["WIS"] <= max))


def filter_cha(df, min, max):
    return df.filter((df["CHA"] >= min) & (df["CHA"] <= max))


def filter_languages(df, language):
    return df.filter(array_contains(col("languages"), language))
