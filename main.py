from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from frontend import *
import json


def main():
    conf = SparkConf().setAppName('Spell Finder')
    sc = SparkContext(conf=conf)
    sc.setLogLevel("ERROR")
    spark = SparkSession(sc)

    data = json.load(open("results/advanced_monsters_results.json"))
    # df = sc.parallelize(data).toDF()

    gui = GUI(sc, data)

    gui.root.mainloop()


if __name__ == "__main__":
    main()
