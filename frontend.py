from tkinter import *
from tkinter import ttk
from map_reduce import *
import webbrowser

# --- Constants ---
RESULT_WINDOW_SIZE = "1102x480"  # "1320x720"
SEARCH_WINDOW_SIZE = "700x670"


class GUI:
    def __init__(self, sc, data):

        self.sc = sc
        self.data = data
        self.df = self.makeDF()

        # --- root object and window parameters ---
        self.root = Tk()
        self.root.title("D&D Creatures Path Finder")
        self.root.iconphoto(True, PhotoImage(file='ressources/D&D_icon.png'))
        self.root.geometry(SEARCH_WINDOW_SIZE)

        self.tree = None

        # --- variables pour items frontend ---

        # check boxes
        self.name_checked = IntVar()
        self.description_checked = IntVar()
        self.type_checked = IntVar()
        self.speed_checked = IntVar()
        self.range_checked = IntVar()
        self.special_attacks_checked = IntVar()
        self.spells_checked = IntVar()
        self.ac_checked = IntVar()
        self.hp_checked = IntVar()
        self.sr_checked = IntVar()
        self.immunity_checked = IntVar()
        self.str_checked = IntVar()
        self.dex_checked = IntVar()
        self.con_checked = IntVar()
        self.int_checked = IntVar()
        self.wis_checked = IntVar()
        self.cha_checked = IntVar()
        self.languages_checked = IntVar()

        self.melee_checked = IntVar()
        self.ranged_checked = IntVar()

        self.types = {
            "aberration": StringVar(),
            "animal": StringVar(),
            "construct": StringVar(),
            "dragon": StringVar(),
            "fey": StringVar(),
            "humanoid": StringVar(),
            "magical_beast": StringVar(),
            "monstrous_humanoid": StringVar(),
            "ooze": StringVar(),
            "outsider": StringVar(),
            "plant": StringVar(),
            "undead": StringVar(),
            "vermin": StringVar()
        }

        # --- General Frame ---
        fg = LabelFrame(self.root, text=" General ")
        fg.grid(row=0, column=0, padx=10, pady=5, sticky=W)

        # Name
        label_name = Label(fg, text="Name :")
        label_name.grid(row=0, column=1, sticky=W)
        self.name = Entry(fg, bg="white", fg="black", width=50)
        self.name.grid(row=0, column=2, columnspan=5, padx=10, sticky=W)

        # Description
        label_description = Label(fg, text="Description :")
        label_description.grid(row=1, column=1, sticky=W)
        self.description = Entry(fg, bg="white", fg="black", width=50)
        self.description.grid(row=1, column=2, columnspan=5, padx=10, sticky=W)

        # Type
        label_type = Label(fg, text="Type :")
        label_type.grid(row=2, column=1, sticky=W)
        cb_aberration = Checkbutton(fg, text="Aberration", var=self.types["aberration"], offvalue="",
                                    onvalue="aberration")
        cb_aberration.grid(row=2, column=2, sticky=W, padx=10)
        cb_animal = Checkbutton(fg, text="Animal", var=self.types["animal"], offvalue="", onvalue="animal")
        cb_animal.grid(row=2, column=3, sticky=W, padx=10)
        cb_construct = Checkbutton(fg, text="Construct", var=self.types["construct"], offvalue="", onvalue="construct")
        cb_construct.grid(row=2, column=4, sticky=W, padx=10)
        cb_dragon = Checkbutton(fg, text="Dragon", var=self.types["dragon"], offvalue="", onvalue="dragon")
        cb_dragon.grid(row=2, column=5, sticky=W, padx=10)
        cb_fey = Checkbutton(fg, text="Fey", var=self.types["fey"], offvalue="", onvalue="fey")
        cb_fey.grid(row=2, column=6, sticky=W, padx=10)
        cb_humanoid = Checkbutton(fg, text="Humanoid", var=self.types["humanoid"], offvalue="", onvalue="humanoid")
        cb_humanoid.grid(row=3, column=2, sticky=W, padx=10)
        cb_magical_beast = Checkbutton(fg, text="Magical Beast", var=self.types["magical_beast"], offvalue="",
                                       onvalue="magical beast")
        cb_magical_beast.grid(row=3, column=3, sticky=W, padx=10)
        cb_monstrous_humanoid = Checkbutton(fg, text="Monstrous Humanoid", var=self.types["monstrous_humanoid"],
                                            offvalue="", onvalue="monstrous humanoid")
        cb_monstrous_humanoid.grid(row=3, column=4, sticky=W, padx=10)
        cb_ooze = Checkbutton(fg, text="Ooze", var=self.types["ooze"], offvalue="", onvalue="ooze")
        cb_ooze.grid(row=3, column=5, sticky=W, padx=10)
        cb_outsider = Checkbutton(fg, text="Outsider", var=self.types["outsider"], offvalue="", onvalue="oustider")
        cb_outsider.grid(row=3, column=6, sticky=W, padx=10)
        cb_plant = Checkbutton(fg, text="Plant", var=self.types["plant"], offvalue="", onvalue="plant")
        cb_plant.grid(row=4, column=2, sticky=W, padx=10)
        cb_undead = Checkbutton(fg, text="Undead", var=self.types["undead"], offvalue="", onvalue="undead")
        cb_undead.grid(row=4, column=3, sticky=W, padx=10)
        cb_vermin = Checkbutton(fg, text="Vermin", var=self.types["vermin"], offvalue="", onvalue="vermin")
        cb_vermin.grid(row=4, column=4, sticky=W, padx=10)

        # --- Offense Frame ---
        fo = LabelFrame(self.root, text=" Offense ")
        fo.grid(row=1, column=0, padx=10, pady=5, sticky=W)

        # Speed
        label_speed = Label(fo, text="Speed :")
        label_speed.grid(row=0, column=1, sticky=W)
        labeld_speed_between = Label(fo, text=" Between ")
        labeld_speed_and = Label(fo, text=" And ")
        labeld_speed_between.grid(row=0, column=2, sticky=W, padx=10)
        labeld_speed_and.grid(row=0, column=4, sticky=W)
        self.min_speed = Entry(fo, bg="white", fg="black", width=10)
        self.max_speed = Entry(fo, bg="white", fg="black", width=10)
        self.min_speed.grid(row=0, column=3, columnspan=1, sticky=W)
        self.max_speed.grid(row=0, column=6, columnspan=1, sticky=W)

        # Range
        label_range = Label(fo, text="Range :")
        label_range.grid(row=1, column=1, sticky=W)
        cb_melee = Checkbutton(fo, text="Melee", padx=10)
        cb_ranged = Checkbutton(fo, text="Ranged")
        cb_melee.grid(row=1, column=2, sticky=W)
        cb_ranged.grid(row=1, column=3, sticky=W)

        # Special Attacks
        label_special_attacks = Label(fo, text="Special Attacks :")
        label_special_attacks.grid(row=2, column=1, sticky=W)
        self.special_attack = Entry(fo, bg="white", fg="black", width=50)
        self.special_attack.grid(row=2, column=2, columnspan=5, padx=10, sticky=W)

        # Spells
        label_spells = Label(fo, text="Spells :")
        label_spells.grid(row=3, column=1, sticky=W)
        self.spell = Entry(fo, bg="white", fg="black", width=50)
        self.spell.grid(row=3, column=2, columnspan=5, padx=10, sticky=W)

        # --- Defense Frame ---
        fd = LabelFrame(self.root, text=" Defense ")
        fd.grid(row=2, column=0, padx=10, pady=5, sticky=W)

        # AC
        label_ac = Label(fd, text="AC :")
        label_ac.grid(row=0, column=1, sticky=W)
        label_ac_between = Label(fd, text=" Between ")
        label_ac_and = Label(fd, text=" And ")
        label_ac_between.grid(row=0, column=2, sticky=W, padx=10)
        label_ac_and.grid(row=0, column=4, sticky=W)
        self.min_ac = Entry(fd, bg="white", fg="black", width=10)
        self.max_ac = Entry(fd, bg="white", fg="black", width=10)
        self.min_ac.grid(row=0, column=3, columnspan=1, padx=10, sticky=W)
        self.max_ac.grid(row=0, column=6, columnspan=1, padx=10, sticky=W)

        # HP
        label_hp = Label(fd, text="HP :")
        label_hp.grid(row=1, column=1, sticky=W)
        label_hp_between = Label(fd, text=" Between ")
        label_hp_and = Label(fd, text=" And ")
        label_hp_between.grid(row=1, column=2, sticky=W, padx=10)
        label_hp_and.grid(row=1, column=4, sticky=W)
        self.min_hp = Entry(fd, bg="white", fg="black", width=10)
        self.max_hp = Entry(fd, bg="white", fg="black", width=10)
        self.min_hp.grid(row=1, column=3, columnspan=1, padx=10, sticky=W)
        self.max_hp.grid(row=1, column=6, columnspan=1, padx=10, sticky=W)

        # SR
        label_sr = Label(fd, text="SR :")
        label_sr.grid(row=2, column=1, sticky=W)
        label_sr_between = Label(fd, text=" Between ")
        label_sr_and = Label(fd, text=" And ")
        label_sr_between.grid(row=2, column=2, sticky=W, padx=10)
        label_sr_and.grid(row=2, column=4, sticky=W)
        self.min_sr = Entry(fd, bg="white", fg="black", width=10)
        self.max_sr = Entry(fd, bg="white", fg="black", width=10)
        self.min_sr.grid(row=2, column=3, columnspan=1, padx=10, sticky=W)
        self.max_sr.grid(row=2, column=6, columnspan=1, padx=10, sticky=W)

        # Immunity
        label_immunity = Label(fd, text="Immunity :")
        label_immunity.grid(row=3, column=1, sticky=W)
        self.immunity = StringVar()
        om_immunity = OptionMenu(fd, self.immunity, "fire", "acid", "blindness", "cold", "critical hits",
                                 "curse effects",
                                 "death effects", "disease", "electricity", "magic", "mind-affecting effects",
                                 "paralysis",
                                 "petrification", "sleep", "weapon damage")
        om_immunity.grid(row=3, column=2, columnspan=4, sticky=W, padx=10)

        # --- Statistics Frame ---
        fs = LabelFrame(self.root, text=" Statistics ")
        fs.grid(row=3, column=0, padx=10, pady=5, sticky=W)

        # STR
        label_str = Label(fs, text="STR :")
        label_str.grid(row=0, column=1, sticky=W)
        label_str_between = Label(fs, text=" Between ")
        label_str_and = Label(fs, text=" And ")
        label_str_between.grid(row=0, column=2, sticky=W, padx=10)
        label_str_and.grid(row=0, column=4, sticky=W)
        self.min_str = Entry(fs, bg="white", fg="black", width=10)
        self.max_str = Entry(fs, bg="white", fg="black", width=10)
        self.min_str.grid(row=0, column=3, columnspan=1, padx=10, sticky=W)
        self.max_str.grid(row=0, column=6, columnspan=1, padx=10, sticky=W)

        # DEX
        label_dex = Label(fs, text="DEX :")
        label_dex.grid(row=1, column=1, sticky=W)
        label_dex_between = Label(fs, text=" Between ")
        label_dex_and = Label(fs, text=" And ")
        label_dex_between.grid(row=1, column=2, sticky=W, padx=10)
        label_dex_and.grid(row=1, column=4, sticky=W)
        self.min_dex = Entry(fs, bg="white", fg="black", width=10)
        self.max_dex = Entry(fs, bg="white", fg="black", width=10)
        self.min_dex.grid(row=1, column=3, columnspan=1, padx=10, sticky=W)
        self.max_dex.grid(row=1, column=6, columnspan=1, padx=10, sticky=W)

        # CON
        label_con = Label(fs, text="CON :")
        label_con.grid(row=2, column=1, sticky=W)
        label_con_between = Label(fs, text=" Between ")
        label_con_and = Label(fs, text=" And ")
        label_con_between.grid(row=2, column=2, sticky=W, padx=10)
        label_con_and.grid(row=2, column=4, sticky=W)
        self.min_con = Entry(fs, bg="white", fg="black", width=10)
        self.max_con = Entry(fs, bg="white", fg="black", width=10)
        self.min_con.grid(row=2, column=3, columnspan=1, padx=10, sticky=W)
        self.max_con.grid(row=2, column=6, columnspan=1, padx=10, sticky=W)

        # INT
        label_int = Label(fs, text="INT :")
        label_int.grid(row=3, column=1, sticky=W)
        label_int_between = Label(fs, text=" Between ")
        label_int_and = Label(fs, text=" And ")
        label_int_between.grid(row=3, column=2, sticky=W, padx=10)
        label_int_and.grid(row=3, column=4, sticky=W)
        self.min_int = Entry(fs, bg="white", fg="black", width=10)
        self.max_int = Entry(fs, bg="white", fg="black", width=10)
        self.min_int.grid(row=3, column=3, columnspan=1, padx=10, sticky=W)
        self.max_int.grid(row=3, column=6, columnspan=1, padx=10, sticky=W)

        # WIS
        label_wis = Label(fs, text="WIS :")
        label_wis.grid(row=4, column=1, sticky=W)
        label_wis_between = Label(fs, text=" Between ")
        label_wis_and = Label(fs, text=" And ")
        label_wis_between.grid(row=4, column=2, sticky=W, padx=10)
        label_wis_and.grid(row=4, column=4, sticky=W)
        self.min_wis = Entry(fs, bg="white", fg="black", width=10)
        self.max_wis = Entry(fs, bg="white", fg="black", width=10)
        self.min_wis.grid(row=4, column=3, columnspan=1, padx=10, sticky=W)
        self.max_wis.grid(row=4, column=6, columnspan=1, padx=10, sticky=W)

        # CHA
        label_cha = Label(fs, text="CHA :")
        label_cha.grid(row=5, column=1, sticky=W)
        label_cha_between = Label(fs, text=" Between ")
        label_cha_and = Label(fs, text=" And ")
        label_cha_between.grid(row=5, column=2, sticky=W, padx=10)
        label_cha_and.grid(row=5, column=4, sticky=W)
        self.min_cha = Entry(fs, bg="white", fg="black", width=10)
        self.max_cha = Entry(fs, bg="white", fg="black", width=10)
        self.min_cha.grid(row=5, column=3, columnspan=1, padx=10, sticky=W)
        self.max_cha.grid(row=5, column=6, columnspan=1, padx=10, sticky=W)

        # Languages
        label_language = Label(fs, text="Languages :")
        label_language.grid(row=6, column=1, sticky=W)
        self.language = StringVar()
        om_language = OptionMenu(fs, self.language, "Aboleth", "Abyssal", "Aklo", "Aquan", "Auran", "Boggard",
                                 "Celestial",
                                 "Common", "Cyclops", "Dark Folk", "Draconic", "Drow Sign Language", "Druidic",
                                 "Dwarven",
                                 "D’ziriak", "Elven", "Giant", "Gnoll", "Gnome", "Goblin", "Grippli", "Halfling",
                                 "Ignan",
                                 "Infernal", "Necril", "Orc", "Protean", "Sphinx", "Sylvan", "Tengu", "Terran",
                                 "Treant",
                                 "Undercommon", "Vegepygmy")
        om_language.grid(row=6, column=2, columnspan=4, sticky=W, padx=10)

        # --- Checkboxes Selection ---
        # General
        cb_name = Checkbutton(fg, variable=self.name_checked)
        cb_name.grid(row=0, column=0, sticky=W)
        cb_description = Checkbutton(fg, variable=self.description_checked)
        cb_description.grid(row=1, column=0, sticky=W)
        cb_type = Checkbutton(fg, variable=self.type_checked)
        cb_type.grid(row=2, column=0, sticky=W)

        # Offense
        cb_speed = Checkbutton(fo, variable=self.speed_checked)
        cb_speed.grid(row=0, column=0, sticky=W)
        cb_range = Checkbutton(fo, variable=self.range_checked)
        cb_range.grid(row=1, column=0, sticky=W)
        cb_special_attacks = Checkbutton(fo, variable=self.special_attacks_checked)
        cb_special_attacks.grid(row=2, column=0, sticky=W)
        cb_spells = Checkbutton(fo, variable=self.spells_checked)
        cb_spells.grid(row=3, column=0, sticky=W)

        # Defense
        cb_ac = Checkbutton(fd, variable=self.ac_checked)
        cb_ac.grid(row=0, column=0, sticky=W)
        cb_hp = Checkbutton(fd, variable=self.hp_checked)
        cb_hp.grid(row=1, column=0, sticky=W)
        cb_sr = Checkbutton(fd, variable=self.sr_checked)
        cb_sr.grid(row=2, column=0, sticky=W)
        cb_immune = Checkbutton(fd, variable=self.immunity_checked)
        cb_immune.grid(row=3, column=0, sticky=W)

        # Statistics
        cb_str = Checkbutton(fs, variable=self.str_checked)
        cb_str.grid(row=0, column=0, sticky=W)
        cb_dex = Checkbutton(fs, variable=self.dex_checked)
        cb_dex.grid(row=1, column=0, sticky=W)
        cb_con = Checkbutton(fs, variable=self.con_checked)
        cb_con.grid(row=2, column=0, sticky=W)
        cb_int = Checkbutton(fs, variable=self.int_checked)
        cb_int.grid(row=3, column=0, sticky=W)
        cb_wis = Checkbutton(fs, variable=self.wis_checked)
        cb_wis.grid(row=4, column=0, sticky=W)
        cb_cha = Checkbutton(fs, variable=self.cha_checked)
        cb_cha.grid(row=5, column=0, sticky=W)
        cb_languages = Checkbutton(fs, variable=self.languages_checked)
        cb_languages.grid(row=6, column=0, sticky=W)

        # --- Search Button ---
        searchButton = Button(self.root, text='Go Find!', width=95, bg="#6F8FAF", fg="white",
                              command=self.go_find_clicked)
        searchButton.grid(row=4, column=0, padx=10, pady=5)

    def makeDF(self):
        return self.sc.parallelize(self.data).toDF()

    def on_double_click(self, event):
        item = self.tree.identify('item', event.x, event.y)
        print(self.tree.item(item, "value")[11])
        webbrowser.open(self.tree.item(item, "value")[11])

    def go_find_clicked(self):
        self.df = self.makeDF()

        if self.name_checked.get():
            self.df = filter_name(self.df, self.name.get())

        if self.description_checked.get():
            self.df = filter_description(self.df, self.description.get())

        if self.type_checked.get():
            self.df = filter_type(self.df, self.types)

        if self.speed_checked.get():
            self.df = filter_speed(self.df, self.min_speed.get(), self.max_speed.get())

        if self.range_checked.get():
            self.df = filter_range(self.df, self.melee_checked.get(), self.ranged_checked.get())

        if self.special_attacks_checked.get():
            self.df = filter_special_attacks(self.df, self.special_attack.get())

        if self.spells_checked.get():
            self.df = filter_spells(self.df, self.spell.get())

        if self.ac_checked.get():
            self.df = filter_ac(self.df, self.min_ac.get(), self.max_ac.get())

        if self.hp_checked.get():
            self.df = filter_hp(self.df, self.min_hp.get(), self.max_hp.get())

        if self.sr_checked.get():
            self.df = filter_sr(self.df, self.min_sr.get(), self.max_sr.get())

        if self.immunity_checked.get():
            self.df = filter_immunities(self.df, self.immunity.get())

        if self.str_checked.get():
            self.df = filter_str(self.df, self.min_str.get(), self.max_str.get())

        if self.dex_checked.get():
            self.df = filter_dex(self.df, self.min_dex.get(), self.max_dex.get())

        if self.con_checked.get():
            self.df = filter_con(self.df, self.min_con.get(), self.max_con.get())

        if self.int_checked.get():
            self.df = filter_int(self.df, self.min_int.get(), self.max_int.get())

        if self.wis_checked.get():
            self.df = filter_wis(self.df, self.min_wis.get(), self.max_wis.get())

        if self.cha_checked.get():
            self.df = filter_cha(self.df, self.min_cha.get(), self.max_cha.get())

        if self.languages_checked.get():
            self.df = filter_languages(self.df, self.language.get())

        self.df = self.df.orderBy('name').cache()

        #print(f"Total number of rows: {self.df.count()}")
        #self.df.show()

        # --- root object and window parameters ---
        result_window = Tk()
        result_window.title("results")
        result_window.geometry(RESULT_WINDOW_SIZE)

        columns = ('name', 'type', 'HP', 'AC', 'speed', 'STR', 'DEX', 'CON', 'INT', 'WIS', 'CHA', 'link')

        self.tree = ttk.Treeview(result_window, columns=columns, show='headings', height=100)

        self.tree.column('name', width=250)
        self.tree.heading('name', text='Name')

        self.tree.column('type', width=200)
        self.tree.heading('type', text='Type')

        self.tree.column('HP', width=50)
        self.tree.heading('HP', text='HP')

        self.tree.column('AC', width=50)
        self.tree.heading('AC', text='AC')

        self.tree.column('speed', width=50)
        self.tree.heading('speed', text='Speed')

        self.tree.column('STR', width=50)
        self.tree.heading('STR', text='STR')

        self.tree.column('DEX', width=50)
        self.tree.heading('DEX', text='DEX')

        self.tree.column('CON', width=50)
        self.tree.heading('CON', text='CON')

        self.tree.column('INT', width=50)
        self.tree.heading('INT', text='INT')

        self.tree.column('WIS', width=50)
        self.tree.heading('WIS', text='WIS')

        self.tree.column('CHA', width=50)
        self.tree.heading('CHA', text='CHA')

        self.tree.column('link', width=200)
        self.tree.heading('link', text='link')

        self.filtered_rdd = self.df.rdd.collect()

        for result in self.filtered_rdd:
            self.tree.insert('', 'end', values=(
                result.name, result.type, result.HP, result.AC, result.speed, result.STR, result.DEX, result.CON,
                result.INT, result.WIS,
                result.CHA,
                result.link))

        self.tree.bind('<Double-1>', self.on_double_click)
        self.tree.pack()
